<%@ include file="/WEB-INF/views/includes/references.jsp"%>
<% String home = ("".equals(request.getContextPath())) ? "/" : request.getContextPath();%>
<div class="row ml-0 mr-0">
	<div class="col-md-12 pl-0 pr-0">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="<%=home%>">
						<font face="lato black" color=red>
							<strong>TechDiggity</strong>
						</font>
					</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="<%=home%>">Home</a></li>
					<li><a href="#">Reset</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<div class="navbar-header" style="float: right">
					<%
						if ("true".equals(session.getAttribute("loggedIn"))) {
							out.println("<a class='navbar-brand' href='"+home+"'>Welcome Back</a>\n");
							out.println("<a style='margin-top: 7px;' href='"+request.getContextPath()+"/logout' class='btn btn-danger'>\n");
							out.println("	Log Out\n");
							out.println("</a>");
						} else {
							out.println("<a style='margin-top: 7px;' class='btn btn-success' href='"+request.getContextPath()+"login'>");		
							out.println("	Login\n");
							out.println("</a>");
						}
					%>
				</div>
			</div>
		</nav>
	</div>
</div>