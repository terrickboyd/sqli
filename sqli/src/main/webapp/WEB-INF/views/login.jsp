<%@ include file="/WEB-INF/views/includes/references.jsp"%>
<!DOCTYPE html>
<html lang="en">
    <%@ include file="includes/head.jsp" %>
    <body>
    	<%@ include file="includes/nav.jsp" %>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 body-content" >
                        <div class="page-header clearfix">
                            <h2 class="pull-left"><font color="red"><b>Login Area</b></font></h2>
                        </div>
                        <form action="<%=request.getContextPath()%>/login" method="post">
                            <div class="form-group">
                                <label><font color="white"><strong>Username</strong></font></label>
                                <input type="text" name="username" class="form-control" />
                            </div>
                            <div class="form-group ">
                                <label><font color="white"><strong>Password</strong></font></label>
                                <input type="password" name="password" class="form-control" />
                            </div>
                            <input type="submit" class="btn btn-primary" value="Login">
                        </form>
                    </div>
                </div>        
            </div>
        </div>
        <%@ include file="includes/footer.jsp" %>
    </body>
</html>