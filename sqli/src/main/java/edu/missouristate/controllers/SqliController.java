package edu.missouristate.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.missouristate.service.SqliService;

@Controller
public class SqliController {

	@Autowired
	SqliService sqliService;
	
	@GetMapping(value="/")
	public String getIndex(HttpSession session, Model model) {
		if ("true".equals(session.getAttribute("loggedIn"))) {
			model.addAttribute("reviewList", sqliService.getReviewList());
			return "index";
		}
		
		return "login";
	}
	
	@GetMapping(value="/login")
	public String getLogin(HttpSession session, Model model) {
		// If the user is already logged in, take them to the index page
		if ("true".equals(session.getAttribute("loggedIn"))) {
			model.addAttribute("reviewList", sqliService.getReviewList());
			return "index";
		}
		
		return "login";
	}
	
	@PostMapping(value="/login")
	public String getPostMapping(HttpSession session, String username, String password) {
		
		boolean authenticated = false;
		
		// Check credentials
		authenticated = sqliService.isAuthenticated(username, password);
		
//		if ("jlb".equalsIgnoreCase(username) && "515".equals(password)) {
//			authenticated = true;
//		}
		
		// If authenticated, set session variable "loggedIn" to true
		if (authenticated) {
			session.setAttribute("loggedIn", "true");
		}
		
		// Redirect to application index (they will see login.jsp or index.jsp)
		return "redirect:/";
	}
	
	@GetMapping(value="/logout")
	public String getLogout(HttpSession session) {
		session.removeAttribute("loggedIn");
		return "redirect:/";
	}
	
	@GetMapping(value="/addEditReview")
	public String getAddEditReview(HttpSession session, @RequestParam(required = false) Integer id) {
		if (id == null) {
			// Create new Review
			
		} else {
			// Edit Review
			
			
		}

		return "addEditReview";
	}
	
}
