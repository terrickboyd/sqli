package edu.missouristate.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.MSU;
import edu.missouristate.domain.Review;

@Repository
public class SqliRepository {

	@Autowired
	JdbcTemplate template;

	public Boolean isAuthenticated(String username, String password) {
		String sql = "SELECT count(*) " +
	                 "  FROM login " +
				     " WHERE username = '" + username + "' " +
	                 "   AND password = '" + password + "' ";
		return template.queryForObject(sql, Integer.class) > 0;
	}

	public List<Review> getReviewList() {
		return (List<Review>)template.query("SELECT * FROM reviews", MSU.REVIEW_BPRM);
	}
	
}
